package com.example.mediarecorder_ex1;

import java.io.IOException;
import java.text.SimpleDateFormat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class Preview extends SurfaceView implements SurfaceHolder.Callback {
	/**
	 * Instance Of MediaRecorder
	 */
	MediaRecorder m_mediaRecorder;

	/**
	 * Instance Of SurfaceHolder
	 */
	SurfaceHolder m_sufaceHolder;

	/**
	 * Instance Of Recoding file Path
	 */
	String m_recPath = null;

	/**
	 * Instance Of Recoding filename
	 */
	String m_recFile = null;
	Camera camera;
	/**
	 * Check Flag Of Recording Status
	 */
	boolean m_bNowRecording = false;

	/**
	 * ������ �Դϴ�.<br/>
	 * MediaRecorder ������ �Ҵ��� ������� �־���մϴ�.<br/>
	 * 
	 * @param context
	 */
	@SuppressLint("NewApi")
	public Preview(Context context) {
		super(context);
		
		camera = Camera.open();
		camera.setDisplayOrientation(90);
		camera.unlock();
		
		m_sufaceHolder = getHolder();
		// m_sufaceHolder.setFixedSize(320, 240);
		m_sufaceHolder.addCallback(this);
		m_sufaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		m_mediaRecorder = new MediaRecorder();

		SimpleDateFormat saveDate = new SimpleDateFormat("yyyyMMdd_HHmmss");
		
		//m_recPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		m_recPath = "/sdcard/UWV/VideoData/";
		m_recFile = saveDate.format(new java.util.Date()) + "_video_data.mp4";

		// Source ����
		m_mediaRecorder.setCamera(camera);
		m_mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		m_mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

		// ��� ���� ����
		m_mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

		// ���ڴ� ����
		m_mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		m_mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);

		// ���� �ɼ� ����
		m_mediaRecorder.setVideoSize(1280, 720);
		m_mediaRecorder.setVideoFrameRate(30);
		// m_mediaRecorder.setMaxDuration(60000);

	}

	/**
	 * SurfaceHolder �� º~ �ϰ� �������� �����մϴ�.
	 * 
	 * @see android.view.SurfaceHolder.Callback#surfaceCreated(android.view.SurfaceHolder)
	 */
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		m_recFile = m_recPath + "/" + m_recFile;
		m_mediaRecorder.setOutputFile(m_recFile);
		// �̸����� ����
		m_mediaRecorder.setPreviewDisplay(m_sufaceHolder.getSurface());
		if (m_mediaRecorder != null) {
			try {
				m_mediaRecorder.prepare();
			} catch (IllegalStateException e) {
				Log.d("yeongeon", "==[A]====>" + e.toString());
			} catch (IOException e) {
				Log.d("yeongeon", "==[B]====>" + e.toString());
			}
		}
	}

	/**
	 * SurfaceHolder �� ����ɶ� �����մϴ�.
	 * 
	 * @see android.view.SurfaceHolder.Callback#surfaceDestroyed(android.view.SurfaceHolder)
	 */
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
	}

	/**
	 * SurfaceHolder �� ���� ����ɶ� �����մϴ�.
	 * 
	 * @see android.view.SurfaceHolder.Callback#surfaceChanged(android.view.SurfaceHolder,
	 *      int, int, int)
	 */
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	/**
	 * �̵�� ���ڴ��� ��ȯ�մϴ�.
	 * 
	 * @return
	 */
	public MediaRecorder getRecorder() {
		return m_mediaRecorder;
	}

	/**
	 * �̵�� ���ڴ��� �Ҵ��մϴ�.
	 * 
	 * @param mediaRecorder
	 */
	public void setRecorder(MediaRecorder mediaRecorder) {
		m_mediaRecorder = mediaRecorder;
	}

	/**
	 * ��ȭ�� �����մϴ�.
	 */
	public void start() {
		m_mediaRecorder.start();
		m_bNowRecording = true;
	}

	/**
	 * ��ȭ�� �����մϴ�.
	 */
	public void stop() {
		try {
			m_mediaRecorder.stop();
		} catch (IllegalStateException e) {
			Log.d("yeongeon", "==[G]====>" + e.toString());
		}
		// m_mediaRecorder.release();
		// m_mediaRecorder = null;
		m_bNowRecording = false;
	}
}
