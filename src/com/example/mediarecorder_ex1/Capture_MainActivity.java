package com.example.mediarecorder_ex1;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


@SuppressLint("NewApi")
public class Capture_MainActivity extends Activity implements SensorEventListener{

    /**
     * Camera Preview Instance 
     */
    Preview m_preview = null;
    
    /**
     * Camera Preview Layout Instance 
     */
    LinearLayout m_previewLayout = null;

    /**
     * Start Action Button Instance  
     */
    Button m_btnStart = null;
    
    /**
     * Stop Action Button Instance  
     */
    Button m_btnStop = null;

    
    //sensor variables
    
    private int checkNum = 1;
    private SensorManager mSensorManager;  // 센서매니저
    private Sensor mAccelerometer, mField; 
    private TextView valueView, directionView;
    private TextView division_frame;
    private Boolean check = false;
    float[] values = new float[3];

   
    private Rate rate;
      
    SimpleDateFormat saveDate = new SimpleDateFormat("yyyyMMdd_HHmmss");
    
	String str_Path_Full = Environment.getExternalStorageDirectory()
			.getAbsolutePath() + "/UWV/SensorData/"+ saveDate.format(new java.util.Date()) +"_sensor_data.txt";
	File file = new File(str_Path_Full);
	

    private float[] mGravity;
    private float[] mMagnetic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capturemain);

        rate = (Rate) getIntent().getSerializableExtra("rate");
        
        // Define Instance Of Camera Preview 
        // And addView
        m_preview = new Preview(this);
        m_previewLayout = (LinearLayout) findViewById(R.id.preview);
        m_previewLayout.addView(m_preview);

        // Define Instance Of Action Buttons
        m_btnStart = (Button) this.findViewById(R.id.btn_start);
        m_btnStop = (Button) this.findViewById(R.id.btn_stop);
        m_btnStop.setClickable(false);
        
        //sensor variable
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER); //가속도계 측정
        mField = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        
        try {
        	file.delete();
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        // Add ClickListeners
        m_btnStart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start Recording
                onRecStart(); 
            }
        });
        m_btnStop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Stop Recording
				onRecStop();
				check=false;
            }
        });
       
    }
   
    
    public void timeThread() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					if (check == true) {
						try {
							loadTxt(Float.toString(values[0]),
									Float.toString(values[1]),
									Float.toString(values[2]));
							Log.i(Float.toString(values[0]),
									Float.toString(values[1]));
							Thread.sleep(1000/rate.getRate());
							
					
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					} else {
						break;
					}
				}
			}
		}).start();
	}

//	public void timeCheckTrhead() {
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				try {
//					Thread.sleep(10000);
//					onRecStop();
//					check=false;
//					
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//
//			}
//		}).start();
//	}
//    
    public void loadTxt(String value1, String value2, String value3){
    
		try {
			BufferedWriter bfw = new BufferedWriter(new FileWriter(
					str_Path_Full, true));
			bfw.write("Number " + checkNum + ": " + value1 + "  " + value2
					+ "  " + value3);
			bfw.write("\n");
			bfw.flush();
			bfw.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		checkNum++;
    }
    
   
    @Override
	protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);//가속도계, 방향전환(rolling)
        mSensorManager.registerListener(this, mField, SensorManager.SENSOR_DELAY_UI); 
    }

    @Override
	protected void onPause() {
        super.onPause();
        onRecStop();
        mSensorManager.unregisterListener(this);
    }

	
    
	public void test(TextView up)
	{
		division_frame = up;
	}
	
    private void updateDirection() {
        float[] temp = new float[9];
        float[] R = new float[9];
        //Load rotation matrix into R
        SensorManager.getRotationMatrix(temp, null, mGravity, mMagnetic);//중력과 자기장을 받아온다.
        //Remap to camera's point-of-view
        SensorManager.remapCoordinateSystem(temp, SensorManager.AXIS_X, SensorManager.AXIS_Z, R); //x축 z푹을 조정
        //Return the orientation values
        
        SensorManager.getOrientation(R, values);
        //Convert to degrees
        for (int i=0; i < values.length; i++) {
            Double degrees = (values[i] * 180) / Math.PI;//각도 계산.
            values[i] = degrees.floatValue();
        }
    }
   
    
    @Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    @Override
	public void onSensorChanged(SensorEvent event) {
        switch(event.sensor.getType()) {
        case Sensor.TYPE_ACCELEROMETER:
            mGravity = event.values.clone(); 
            break;
        case Sensor.TYPE_MAGNETIC_FIELD:
            mMagnetic = event.values.clone();
            break;
        default:
            return;
        }
        
        if(mGravity != null && mMagnetic != null) {
            updateDirection(); //중력에 맞게 방향을 구분
        }
    }
    
    public void Thread(){
    	
    }

    private void onRecStart(){
    	check = true;
        m_preview.start();
//    	timeCheckTrhead();
    	timeThread();
        m_btnStart.setClickable(false);
        m_btnStop.setClickable(true);
        
    }
    
    private void onRecStop(){
        m_preview.stop();
        m_btnStart.setClickable(true);
        m_btnStop.setClickable(false);        
    }


}

