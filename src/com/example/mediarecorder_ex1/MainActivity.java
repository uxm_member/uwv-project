package com.example.mediarecorder_ex1;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity{
	Button m_start = null;
	Button m_gallery = null;
	Button m_setting = null;
	private Rate rate = null;
	int set_rate = 0;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
        setContentView(R.layout.acticity_main);
        
        //Buttons
        m_start = (Button) this.findViewById(R.id.start);
        m_gallery = (Button) this.findViewById(R.id.gallery);
        m_setting = (Button) this.findViewById(R.id.setting);
          
        
        rate = new Rate(10); //Default Set Value = 10
        
        //Add ClickListeners
        m_start.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), Capture_MainActivity.class);
				i.putExtra("rate", rate);
				startActivity(i);	
			}
		});
        
        m_setting.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				DialogSelectOption();
			}
		});
	}
	
	@SuppressLint("ShowToast")
	private void DialogSelectOption(){
		final String items[] = {"10", "20", "30"};
		AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.this);
		ad.setTitle("Setting");
		ad.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // 각 리스트를 선택했을때
            	set_rate = Integer.parseInt(items[whichButton]);
            	
            }
        }).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // OK 버튼 클릭시 , 여기서 선택한 값을 메인 Activity 로 넘기면 된다.
            	rate.setRate(set_rate);
            	dialog.cancel();
            	Toast.makeText(MainActivity.this, String.valueOf(rate.getRate()), Toast.LENGTH_LONG);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Cancel 버튼 클릭시
            	dialog.cancel();
            }
        });
		AlertDialog alert = ad.create(); 
		ad.show();
	}
	
}
