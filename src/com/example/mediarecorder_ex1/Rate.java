package com.example.mediarecorder_ex1;

import java.io.Serializable;

public class Rate implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int rate;

	
	public Rate(int rate) {
		super();
		this.rate = rate;
	}
	
	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}
}
